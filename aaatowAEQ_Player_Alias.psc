Scriptname aaatowAEQ_Player_Alias extends ReferenceAlias  

float Property fWaitTime Auto
; 「左手に呪文を装備＋この秒以内に武器をしまう、構える」動作を
; 指定値の間に行うと左手の呪文を外して盾を装備する。
; この機能はCKから0を指定する事で解除出来る。初期値は1.5秒。

int RightHandSlot = 1
int LeftHandSlot = 2
int SpellLeftHandSlot = 0
Form g_Shield
bool g_bHasSpell

Event AEQ_OnWeaponEquip(form right, form left, bool bStart)
; 武器を構える、しまう時に呼び出されるイベント。引数に左右の武器を持つ。
; 両手武器、弓の場合は両方に同じ物が入る。

	Actor Act = self.GetActorReference()
	if (bStart)
		if (g_bHasSpell)
			g_bHasSpell = false
			Act.UnequipSpell((left as Spell), SpellLeftHandSlot)
		endif

		if (right == left)
			return
		elseif (right && !left)
			if (g_Shield)
				if (Act.GetItemCount(g_Shield) > 0)
					Act.EquipItemEx(g_Shield, LeftHandSlot)
				endif
			endif
		endif
	else
		if (fWaitTime)
			if (Act.GetEquippedSpell(0))
				g_bHasSpell = true
				RegisterForSingleUpdate(fWaitTime)
			endif
		endif
		Form akForm = Act.GetEquippedShield() as Form
		if (akForm)
			g_Shield = akForm
			Act.UnEquipItemEx(akForm)
		endif
		return
	endif
endEvent

Event AEQ_OnWeaponForceEquip(int hand, form rightnew, form rightold, form leftnew, form leftold)
; ホットキーにより強制的に装備に入れ替えが起こる時に呼び出されるイベント
; 引数 hand = 0:左手のみ変更/1:右手のみ変更/2:両手共に変更/-1:エラー/未変更(通常は呼び出されない)
; 装備前強制変更後の装備を引数に持つ。

	if (hand == -1)
		return
	endif
	Actor Act = self.GetActorReference()
	if (rightnew && !leftnew)
		if (g_Shield)
			Act.EquipItemEx(g_Shield, LeftHandSlot)
		endif
	endif
endEvent

Event OnUpdate()
	g_bHasSpell = false
endEvent